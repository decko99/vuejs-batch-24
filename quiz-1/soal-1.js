function next_date(tanggal,bulan,tahun) {
    var tanggal, bulan, tahun, tanggalBaru, bulanBaru, tahunBaru;
    var namaBulan;
    var cekBulan =[4,6,9,11];
    var maxHari;

    if(cekBulan.includes(bulan)) {
        maxHari = 30;
    } else if(bulan == 2){
        maxHari = 28;
        if (tahun%4 == 0 && bulan == 2) {
            maxHari = 29;
        }
    } else {
        maxHari = 31;
    }

    if (tanggal < 1 || bulan < 1 || bulan > 12 || tanggal > maxHari ) {
        console.log ("Periksa kembali isian tanggal atau tahun");
    } else {
        if (tanggal == 31 && bulan == 12) {
            tahunBaru = tahun + 1;
            bulanBaru = 1;
            tanggalBaru = 1;
        } else if (tanggal == maxHari){
            tanggalBaru = 1;
            bulanBaru = bulan +1;
            tahunBaru = tahun;
        } else {
            tanggalBaru = tanggal + 1;
            bulanBaru = bulan;
            tahunBaru = tahun;
        }
    }


    switch(bulanBaru) {
        case 1:   { namaBulan = 'Januari'; break; }
        case 2:   { namaBulan = 'Februari'; break; }
        case 3:   { namaBulan = 'Maret'; break; }
        case 4:   { namaBulan = 'April'; break; }
        case 5:   { namaBulan = 'Mei'; break; }
        case 6:   { namaBulan = 'Juni'; break; }
        case 7:   { namaBulan = 'Juli'; break; }
        case 8:   { namaBulan = 'Agustus'; break; }
        case 9:   { namaBulan = 'September'; break; }
        case 10:  { namaBulan = 'Oktober'; break; }
        case 11:  { namaBulan = 'November'; break; }
        case 12:  { namaBulan = 'Desember'; break; }
        default:  { namaBulan = 'Tidak Terdefinisi'; }}


    kembalian = tanggalBaru + " " + namaBulan + " " + tahunBaru;


return console.log(kembalian);
}

next_date(31,12,2020);