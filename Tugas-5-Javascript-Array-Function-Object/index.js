// Soal 1
console.log('Jawaban soal 1 : '); 

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var daftarHewanBaru = daftarHewan.sort();
var i;

// console.log(daftarHewanBaru);

for(i=0;i<daftarHewanBaru.length;i++){
    console.log( daftarHewanBaru[i]);
}


console.log('\n----------------------------------------------------------------------\n');

// ----------------------------------------------------------------------

// Soal 2

console.log('Jawaban soal 2 : '); 

// soal 2
function introduce (data) {
    var hasilKalimat = "Nama saya " +data.name+ " , umur saya " +data.age+ 
    " tahun, alamat saya di " + data.alamat + " , dan saya punya hobby yaitu " +data.hobby;
    return console.log(hasilKalimat)
 }
 
 var data = {name:"John", age:30, alamat:"Jalan Pelesiran", hobby:"Gaming"}
 var perkenalan = introduce(data);
 console.log(perkenalan); 


console.log('\n----------------------------------------------------------------------\n');

// // ----------------------------------------------------------------------

// // Soal 3

console.log('Jawaban soal 3 : '); 

function hitung_huruf_vokal(text) {
    var hurufVokal = 'aiueoAIUEO';
    var hitung = 0;
    var i;

    for (i=0; i< text.length;i++){
        if (hurufVokal.indexOf(text[i]) !== -1) {
            hitung++;
        }
    } 
    return hitung;
}

var hitung_1 = hitung_huruf_vokal("MuhAmmAd");
var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1 , hitung_2); // 3 2


console.log('\n----------------------------------------------------------------------\n');

// // ----------------------------------------------------------------------

// // Soal 4

console.log('Jawaban soal 4 : '); 

function hitung(masukan) {
    var nilai;
    if (Number.isInteger(masukan)) {
        nilai = (masukan*2) - 2;    
    } else {
        console.log("Masukkan angka!");
    }
    

    return nilai;
}

console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));