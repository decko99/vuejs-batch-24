console.log('Jawaban soal 1 : ');

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren.";

var partSatu = pertama.substr(0,5);
var partDua = pertama.substr(12,7);
var partTiga = kedua.substr(0,7);
var partEmpat = kedua.substr(7,11);

var hasil = partSatu.concat(partDua).concat(partTiga).concat(partEmpat.toUpperCase());

console.log(hasil);

console.log('\n----------------------------------------------------------------------\n');

console.log('Jawaban soal 2 : '); 

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var hasilPertama = Number(kataPertama) + Number(kataKedua) * Number(kataKetiga) + Number(kataKeempat);
var hasilKedua = parseInt(kataPertama) + (parseInt(kataKedua) * parseInt(kataKetiga) + parseInt(kataKeempat));
var hasilKetiga = (Number(kataPertama) - Number(kataKedua) - Number(kataKetiga)) * Number(kataKeempat);

var kalimatPertama = 'Alternatif pertama : '+kataPertama+' + '+kataKedua+' x '+kataKetiga+' + '+kataKeempat+' = ';
var kalimatKedua = 'Alternatif kedua : '+kataPertama+' + ('+kataKedua+' x '+kataKetiga+') + '+kataKeempat+' = ';
var kalimatKetiga =  'Alternatif ketiga : ('+kataPertama+' - '+kataKedua+' - '+kataKetiga+') x '+kataKeempat+' = ';

console.log(kalimatPertama+hasilPertama);
console.log(kalimatKedua+(hasilKedua));
console.log(kalimatKetiga+(hasilKetiga));


console.log('\n----------------------------------------------------------------------\n');

console.log('Jawaban soal 3 : ');

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4,14); // do your own! 
var kataKetiga = kalimat.substring(15,18); // do your own! 
var kataKeempat = kalimat.substring(19,24); // do your own! 
var kataKelima = kalimat.substring(25,31); // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);