// soal 1
console.log('Jawaban soal 1 : '); 


let panjang, lebar;
const konstantaKeliling = 2;


var luasPersegi = (panjang,lebar)=> {return panjang * lebar};
var kelilingPersegi = (panjang,lebar) => {return konstantaKeliling*(panjang+lebar)};

console.log(luasPersegi(2,4));
console.log(kelilingPersegi(2,4));



console.log('\n----------------------------------------------------------------------\n');

// ----------------------------------------------------------------------

// Soal 2

console.log('Jawaban soal 2 : '); 

var fullname = (firstName, lastName) => {return firstName + " " + lastName};
var fullNameLiteral = (firstName, lastName) => {return `${firstName} ${lastName}`};


console.log(fullname("William", "Imoh"));

console.log(fullNameLiteral("dubi", "dubidam"));

console.log('\n----------------------------------------------------------------------\n');

// ----------------------------------------------------------------------

// Soal 3

console.log('Jawaban soal 3 : '); 

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }


  const {firstName, lastName, address, hobby} = newObject

console.log(firstName, lastName, address, hobby)


console.log('\n----------------------------------------------------------------------\n');

// ----------------------------------------------------------------------

// Soal 4

console.log('Jawaban soal 4 : '); 


const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = west.concat(east);

const spreadCombined = [...west, ...east];


//Driver Code
console.log(combined);
console.log(spreadCombined);

console.log('\n----------------------------------------------------------------------\n');

// ----------------------------------------------------------------------

// Soal 5

console.log('Jawaban soal 5 : '); 

const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 

var after = `Lorem ${view} dolor sit amet, cosectetur adipiscing elit, ${planet}`;

console.log('ini before: ' + before);
console.log('ini after: ' + after);