// Soal 1
console.log('Jawaban soal 1 : '); 

// buatlah variabel seperti di bawah ini

var nilai = 85;
// pilih angka dari 0 sampai 100, misal 75. 
// lalu isi variabel tersebut dengan angka tersebut. 
// lalu buat lah pengkondisian dengan if-elseif dengan kondisi

// nilai >= 85 indeksnya A
// nilai >= 75 dan nilai < 85 indeksnya B
// nilai >= 65 dan nilai < 75 indeksnya c
// nilai >= 55 dan nilai < 65 indeksnya D
// nilai < 55 indeksnya E

if ( nilai >= 85 ) {
    console.log("A")
} else if ( nilai >=75 && nilai < 85) {
    console.log("B")
} else if ( nilai >=65 && nilai < 75) {
    console.log("C")
} else if ( nilai >=55 && nilai < 65) {
    console.log("D")
} else {
    console.log("E")
}

console.log('\n----------------------------------------------------------------------\n');

// ----------------------------------------------------------------------

// Soal 2

console.log('Jawaban soal 2 : '); 

// soal 2

// buatlah variabel seperti di bawah ini

var tanggal = 22;
var bulan = 12;
var tahun = 2020;
var namaBulan;
var hasil;    
// ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda 
// dan buatlah switch case pada bulan, 
// lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 
// (isi di sesuaikan dengan tanggal lahir masing-masing)

switch(bulan) {
    case 1:   { namaBulan = 'Januari'; break; }
    case 2:   { namaBulan = 'Februari'; break; }
    case 3:   { namaBulan = 'Maret'; break; }
    case 4:   { namaBulan = 'April'; break; }
    case 5:   { namaBulan = 'Mei'; break; }
    case 6:   { namaBulan = 'Juni'; break; }
    case 7:   { namaBulan = 'Juli'; break; }
    case 8:   { namaBulan = 'Agustus'; break; }
    case 9:   { namaBulan = 'September'; break; }
    case 10:  { namaBulan = 'Oktober'; break; }
    case 11:  { namaBulan = 'November'; break; }
    case 12:  { namaBulan = 'Desember'; break; }
    default:  { namaBulan = 'Tidak Terdefinisi'; }}

hasil = tanggal + ' ' + namaBulan + ' ' + tahun;    
console.log(hasil);

console.log('\n----------------------------------------------------------------------\n');

// ----------------------------------------------------------------------

// Soal 3

console.log('Jawaban soal 3 : '); 

var i;
var spasi = '';
var tingkat = 5;

for($i=0;$i<tingkat;$i++){
    console.log( spasi += '# ');
}


console.log('\n----------------------------------------------------------------------\n');

// ----------------------------------------------------------------------

// Soal 4

console.log('Jawaban soal 4 : '); 

var kataPertama = 'I love programming';
var kataKedua =  'I love Javascript';
var kataKetiga = 'I love VueJS';
var i = 1;
var angka = 10;

while ( angka > 0) {
    console.log(i + ' - ' + kataPertama);
    i  ++;
    angka --;
    if (angka>0){
        console.log(i + ' - ' + kataKedua);
        i++;
        angka--;
        if(angka>0) {
            console.log(i + ' - ' + kataKetiga);
            console.log('=========');
            i ++;
        }
    }
    angka--;
}